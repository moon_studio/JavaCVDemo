package com.moonstudio;

import javax.swing.JFrame;

import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Rect;
import org.bytedeco.javacpp.opencv_core.RectVector;
import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.OpenCVFrameConverter;
import org.bytedeco.javacv.OpenCVFrameGrabber;
import static org.bytedeco.javacpp.opencv_imgproc.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacpp.opencv_imgcodecs;
import org.bytedeco.javacpp.opencv_objdetect.CascadeClassifier;

public class Demo1 {

	private static CascadeClassifier cascade;

	public static void main(String[] args) throws Exception, InterruptedException {
		System.out.println("##################开始运行##################");
		try {
			OpenCVFrameGrabber openCVFrameGrabber = new OpenCVFrameGrabber(0);
			openCVFrameGrabber.start();
			// 获取摄像头数据
			CanvasFrame canvasFrame = new CanvasFrame("保持微笑");
			canvasFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			// 新建一个窗口
			canvasFrame.setAlwaysOnTop(true);

			while (true) {
				Frame frame = openCVFrameGrabber.grab();// 获取摄像头图像的一帧
				OpenCVFrameConverter.ToIplImage converter = new OpenCVFrameConverter.ToIplImage();
				Mat scr = converter.convertToMat(frame);// 将获取的frame转化成mat数据类型
				detectFace(scr);// 人脸检测
				frame = converter.convert(scr);// 将检测结果重新的mat重新转化为frame
				canvasFrame.showImage(frame);// 获取摄像头图像并放到窗口上显示，frame是一帧视频图像

				Thread.sleep(500);// 50毫秒刷新一次图像

				if (!canvasFrame.isDisplayable()) {
					// 如果关闭窗口,则关闭摄像头
					openCVFrameGrabber.stop();
					// 退出
					System.exit(0);
				}
				Gc();
			}
		} catch (FrameGrabber.Exception e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void Gc() {
		System.out.println("GCGCGCGCGCGCGCGC释放内存GCGCGCGCGCGCGCGC");
		// 每次循环之后释放内存
		System.gc();
	}

	public static String getFileName() {
		String dateStr = "";
		Date date = new Date();
		// format的格式可以任意
		DateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		try {

			dateStr = sdf2.format(date);
			// System.out.println(dateStr);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dateStr;
	}

	public static Mat detectFace(Mat src) {
		Mat grayscr = new Mat();
		cvtColor(src, grayscr, COLOR_BGRA2GRAY);// 摄像头是彩色图像，所以先灰度化下
		equalizeHist(grayscr, grayscr); // 创建用来装检测出来的人脸的容器
		RectVector faces = new RectVector();
		// 备用lbpcascade_frontalface.xml
		cascade = new CascadeClassifier("haarcascade_frontalface_alt2.xml");
		cascade.detectMultiScale(grayscr, faces);// 检测人脸，grayscr为要检测的图片，faces用来存放检测结果
		if (faces.size() > 0) {
			System.out.println("存储本地文件" + getFileName());
			opencv_imgcodecs.imwrite("face" + getFileName() + ".jpg", src);
			for (int i = 0; i < faces.size(); i++) {
				Rect face_i = faces.get(i);

				rectangle(src, face_i, new Scalar(0, 0, 255, 2));
			}
		} else {
			System.out.println(getFileName() + "未检测到人脸！");
		}

		return src;
	}

}
